filesystem
=========

The filesystem module checks the filesystem-security

Requirements
------------

None

Role Variables
--------------

enable_filesystem: true - [controls if the module will run at all]

Dependencies
------------

None

License
-------

MIT

Author Information
------------------

signits@acme.dev
